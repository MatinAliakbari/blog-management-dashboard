import axios from "axios";

export default {
  namespaced: true,
  state: {
    token: null,
    user: null,
  },
  getters: {
    authenticated(state) {
      return state.token;
    },
    user(state) {
      return state.user;
    },
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_USER(state, user) {
      state.user = user;
    },
  },
  actions: {
    async Register({ dispatch }, payload) {
      let response = await axios.post("api/users", payload);
      return dispatch("setDataUser", response.data.user);
    },
    async Login({ dispatch }, payload) {
      let response = await axios.post("api/users/login", payload);
      return dispatch("setDataUser", response.data.user);
    },

    async getUser({ commit, dispatch }, token) {
      if (token) {
        commit("SET_TOKEN", token);
      } else return;

      let response = await axios.get("api/user");
      return dispatch("setDataUser", response.data.user);
    },

    async setDataUser({ commit }, dataUser) {
      let user = {
        username: dataUser.username,
        email: dataUser.email,
        bio: dataUser.bio,
        image: dataUser.image,
      };

      commit("SET_TOKEN", dataUser.token);
      commit("SET_USER", user);
    },
  },
};
