import { createStore } from "vuex";
import auth from "./authentication/auth";

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
  },
});
