import { createRouter, createWebHistory } from "vue-router";
import TheLogin from "../views/Authentication/TheLogin.vue";

const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    name: "TheLogin",
    component: TheLogin,
  },
  {
    path: "/register",
    name: "TheRegister",
    component: () => import("../views/Authentication/TheRegister.vue"),
  },
  {
    path: "/dashboard",
    name: "TheDashboard",
    redirect: "/dashboard/articles",
    component: () => import("../views/Dashboard/TheDashboard.vue"),
    children: [
      {
        path: "articles",
        name: "DashboardArticlesList",
        component: () => import("../views/Dashboard/Articles/ArticlesList.vue"),
      },
      {
        path: "articles/page/:page",
        name: "DashboardArticlesListPage",
        component: () => import("../views/Dashboard/Articles/ArticlesList.vue"),
      },
      {
        path: "articles/create",
        name: "DashboardArticlesCreate",
        component: () => import("../views/Dashboard/Articles/ArticlesForm.vue"),
      },
      {
        path: "articles/edit/:slug",
        name: "DashboardArticlesEdit",
        component: () => import("../views/Dashboard/Articles/ArticlesForm.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  let userToken = localStorage.getItem("token");
  if (!userToken) {
    if (to.name !== "TheLogin" && to.name !== "TheRegister") {
      next({ name: "TheLogin" });
    } else next();
  } else if (to.name === "TheLogin" || to.name === "TheRegister") {
    next({ name: "TheDashboard" });
  } else next();
});

export default router;
