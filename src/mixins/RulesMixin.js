export default {
  methods: {
    required(value) {
      let errorMessage = "Required field";
      let validate = value && value !== null && value !== "";
      return validate || errorMessage;
    },
    email(value) {
      let errorMessage = "Email is invalid";
      let pattern = /^\S+@\S+\.\S+$/;
      return pattern.test(value) || errorMessage;
    },
  },
};
