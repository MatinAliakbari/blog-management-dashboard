export default {
  methods: {
    notify(message, color, textColor) {
      this.$q.notify({
        message: `${message}`,
        html: true,
        color: color,
        textColor: textColor,
        position: "top-right",
        actions: [
          {
            label: "x",
            color: "grey-7",
          },
        ],
      });
    },

    successNotify(message) {
      this.notify(message, "light-green-2", "light-green-10");
    },
    errorNotify(message) {
      this.notify(message, "red-1", "deep-orange-10");
    },
  },
};
