export default {
  methods: {
    createQueryString(path, params) {
      if (!params) return path;
      Object.keys(params).forEach((parameter) => {
        if (!params[parameter]) return;
        let queryKey = `{${parameter}}`;
        if (path.indexOf(queryKey) !== -1) {
          path = path.replace(queryKey, params[parameter]);
        } else {
          if (path.indexOf("?") === -1) path += "?";
          else path += "&";
          path += `${parameter}=${params[parameter]}`;
        }
      });
      return path;
    },

    async getAllArticles(limit, offset) {
      const route = this.createQueryString("api/articles", {
        limit,
        offset,
      });
      const response = await this.axios.get(route);
      return response.data;
    },
    async getArticle(slug) {
      const route = `api/articles/${slug}`;
      const response = await this.axios.get(route);
      return response.data;
    },
    async createArticle(slug, payload) {
      const route = "api/articles";
      const response = await this.axios.post(route, payload);
      return response.data;
    },
    async updateArticle(slug, payload) {
      const route = `api/articles/${slug}`;
      const response = await this.axios.put(route, payload);
      return response.data;
    },
    async deleteArticle(slug) {
      const route = `api/articles/${slug}`;
      const response = await this.axios.delete(route);
      return response.data;
    },
    async getAllTags() {
      const route = "api/tags";
      const response = await this.axios.get(route);
      return response.data;
    },
  },
};
