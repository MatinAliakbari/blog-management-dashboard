import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { Quasar } from "quasar";
import quasarUserOptions from "./quasar-user-options";
import axios from "axios";
import VueAxios from "vue-axios";
import dayjs from "dayjs";

require("@/store/authentication/subscriber");
axios.defaults.baseURL = "https://api.realworld.io/";

const app = createApp(App);

app.use(Quasar, quasarUserOptions);
app.use(store);
app.use(router);
app.use(VueAxios, axios);
app.config.globalProperties.$dayjs = dayjs;

app.mount("#app");
